﻿namespace Xplorium.Web.Mvc {
    public enum OptimizeMode {
        Html,
        Css,
        Js,
        None
    }
}